# server hacker news api 

## Description
This project shows all news about a specific subject

### Pre-requirements 📋
- Java 11 or higher
- Git installed
- GitBash - download in this https://gitforwindows.org/
- Docker and docker compose
- 
## Setup 📦
- Open terminal or cmd
- Clone the project with the command "git clone https://gitlab.com/personal550/servernewsapi.git
- Navigate to folder called "servernewsapi" with command "cd servernewsapi"
- Run linux command  "./mvnw clean install -DskipTests" or System window Run next command  "mvnw.cmd clean install -DskipTests"
- Run command "docker build -t story-api-docker.jar ."
- Run command "docker-compose up -d"
- To restart the microservice, we need to run the next command again "docker-compose up -d"

## Components diagram
![diagram](img/diagram.png)

## Documentation
- You can visit documentation for more information about the API in this link
    - http://localhost:8080/swagger-ui.html

## Usage
If you prefer, you can read the next guide here : https://www.evernote.com/shard/s589/sh/b10092ea-ce47-4b08-aa07-ca6bd6f6c930/ebe057d767f855e8a5f4d2a9963dc472

Or you can follow the next steps

First we need to create a user to the database with the next end point
    
    -url : http://localhost:8080/api/auth/signup
    -http method: POST
    -request :
    {
        "username":"user1",
        "email":"user1@gmail.com",
        "password":"user1"
    }

![signup](img/signup.png)
    

If we executed the before endpoint, we obtain the follow response if our user was saved successfully

    response :
    {
        "message": "user user1 was registered successfully!"
    }

Second, we need to obtain a token for that we could execute ours endpoints

    - url : http://localhost:8080/api/auth/login
    - http method: POST
    request :
    {
        "username":"user1",
        "password":"user1"
    }

If everything OKs, we will obtain a token for ours endpoints
    
    request:
    {
        "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb3NlIiwiaWF0IjoxNjY4NTMxNDk0LCJleHAiOjE2Njg2MTc4OTR9.qAQJO8FLsBLJAQDa2xz9YbJICvcMRCFBpHDyIn1cW030wfhKUdsMXMqvHmVXtOSxDeogCx5lZKfs2Ty-Gsmodw",
        "type": "Bearer",
        "id": 1,
        "username": "user1",
        "email": "email@gmail.com",
        "roles": [
            "ROLE_ADMIN"
        ]
    }

-- Add our token to endpoint
![authenticate](img/authenticate.png)

So now we could execute ours endpoint the follow way:

For all ours endpoint we need to add a header call Authorization with the token generated before like it shows below
    Bearer <insert_token_generated>
![bearertoken](img/bearertoken.png)


1.- End point for refresh data base
    
    -url : http://localhost:8080/api/v1/story/refresh
    -http method: GET
    -header: Autorization = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb3NlIiwiaWF0IjoxNjY4NTMxNDk0LCJleHAiOjE2Njg2MTc4OTR9.qAQJO8FLsBLJAQDa2xz9YbJICvcMRCFBpHDyIn1cW030wfhKUdsMXMqvHmVXtOSxDeogCx5lZKfs2Ty-Gsmodw"

    -example
![refreshdb](img/refreshdb.png)

2.- Find by author
    
    -url : http://localhost:8080/api/v1/story/author/<name_author_to_search>
    -http method: GET
    -header: Autorization = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb3NlIiwiaWF0IjoxNjY4NTMxNDk0LCJleHAiOjE2Njg2MTc4OTR9.qAQJO8FLsBLJAQDa2xz9YbJICvcMRCFBpHDyIn1cW030wfhKUdsMXMqvHmVXtOSxDeogCx5lZKfs2Ty-Gsmodw"

    -example
![searchbyauthor](img/searchbyauthor.png)

3.- Find by tags
    
    -url : http://localhost:8080/api/v1/story/tags/<list_tags_separated_by_comma>
    -http method: GET
    -header: Autorization = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb3NlIiwiaWF0IjoxNjY4NTMxNDk0LCJleHAiOjE2Njg2MTc4OTR9.qAQJO8FLsBLJAQDa2xz9YbJICvcMRCFBpHDyIn1cW030wfhKUdsMXMqvHmVXtOSxDeogCx5lZKfs2Ty-Gsmodw"

    -example
![searchbytag](img/searchbytag.png)

4.- Find by title
    
    -url : http://localhost:8080/api/v1/story/title/<title>
    -http method: GET
    -header: Autorization = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb3NlIiwiaWF0IjoxNjY4NTMxNDk0LCJleHAiOjE2Njg2MTc4OTR9.qAQJO8FLsBLJAQDa2xz9YbJICvcMRCFBpHDyIn1cW030wfhKUdsMXMqvHmVXtOSxDeogCx5lZKfs2Ty-Gsmodw"

    -example
![searchbytitle](img/searchbytitle.png)

4.- Find by month
For this end point, we just search by the next list of months : JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUN, JULY, AUGUST, NOVEMBER, DECEMBER 
    
    -url : http://localhost:8080/api/v1/story/month/<month>
    -http method: GET
    -header: Autorization = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb3NlIiwiaWF0IjoxNjY4NTMxNDk0LCJleHAiOjE2Njg2MTc4OTR9.qAQJO8FLsBLJAQDa2xz9YbJICvcMRCFBpHDyIn1cW030wfhKUdsMXMqvHmVXtOSxDeogCx5lZKfs2Ty-Gsmodw"

    -example
![searchbymonth](img/searchbymonth.png)

For all endpoints mentioned above, the response will be like next example:

    -response :
        {
            "id": 1,
            "story_id": 33594676,
            "author": "lazide",
            "_tags": [
                "comment",
                "author_lazide",
                "story_33594676"
                ],
            "title": null,
            "created_at": "2022-11-15T21:00:02",
            "story_title": "Making a Go program faster with a one-character change",
            "status": false
        },
        {
            "id": 2,
            "story_id": 33613854,
            "author": "jamal-kumar",
            "_tags": [
                "comment",
                "author_jamal-kumar",
                "story_33613854"
                ],
            "title": null,
            "created_at": "2022-11-15T20:57:57",
            "story_title": "What to do if a nuclear disaster is imminent [pdf]",
            "status": false
        }
    ]

5.- Delete by ID, the ID we could obtain of the response of all end point mentioned before
    
    -url : http://localhost:8080/api/v1/story/<id>
    -http method: DELETE
    -header: Autorization = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb3NlIiwiaWF0IjoxNjY4NTMxNDk0LCJleHAiOjE2Njg2MTc4OTR9.qAQJO8FLsBLJAQDa2xz9YbJICvcMRCFBpHDyIn1cW030wfhKUdsMXMqvHmVXtOSxDeogCx5lZKfs2Ty-Gsmodw"


If the record with the ID exist, we could see a response HTTP 200

    -example
![deletebyid](img/deletebyid.png)

## Build with 🛠️
- Java
- Spring boot
- Docker
- Junit
- Postgrest
- JPA
