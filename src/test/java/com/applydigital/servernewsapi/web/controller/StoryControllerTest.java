package com.applydigital.servernewsapi.web.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.applydigital.servernewsapi.business.service.HackerNewsService;
import com.applydigital.servernewsapi.business.service.StoryService;
import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dto.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class StoryControllerTest {

  @Mock
  private HackerNewsService hackerNewsService;
  @Mock
  private StoryService storyService;

  @InjectMocks
  private StoryController storyController = new StoryController();

  @Test
  void should_returnAHttpStatus204_when_recordWasDeletedSuccessfully(){
    when(storyService.existIdAndStatus(any())).thenReturn(false);
    ResponseEntity response = storyController.deleteById(1l);
    int expected = 204;
    int actual = response.getStatusCode().value();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnStatusCode404_when_recordNotFound(){
    when(storyService.existIdAndStatus(any())).thenReturn(true);
    ResponseEntity response = storyController.deleteById(3L);
    int expected = 404;
    int actual = response.getStatusCode().value();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnAnException_when_serviceThrowAnException(){
    when(storyService.existIdAndStatus(any())).thenThrow(RuntimeException.class);
    try{
      storyController.deleteById(3l);
    }catch(ResponseStatusException e){
      String expected = "500 INTERNAL_SERVER_ERROR \"the id {3} cant delete\"";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }

  @Test
  void should_returnAListNotEmpty_when_existRecordsInBd(){
    when(storyService.findFirst5ByAuthor(any())).thenReturn(new ArrayList<>(){
      {
        add(StoryDto.builder().build());
      }
    });
    ResponseEntity<List<StoryDto>> response = storyController.searchByAuthor("jhon");
    int expected = 1;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(200, response.getStatusCode().value());
  }

  @Test
  void should_returnAListEmpty_when_doesNotExistRecordsInBd(){
    when(storyService.findFirst5ByAuthor(any())).thenReturn(new ArrayList<>());
    ResponseEntity<List<StoryDto>> response = storyController.searchByAuthor("jhon");
    int expected = 0;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(404, response.getStatusCode().value());
  }
  @Test
  void should_returnAnException_when_searchByAuthorThrowAnException(){
    when(storyService.findFirst5ByAuthor(any())).thenThrow(RuntimeException.class);
    try{
      storyController.searchByAuthor("jhon");
    }catch(ResponseStatusException e){
      String expected = "500 INTERNAL_SERVER_ERROR \"the {jhon} not found\"";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }

  @Test
  void should_returnAListNotEmpty_when_searchByTitleExistRecordsInBd(){
    when(storyService.findFirst5ByTitle(any())).thenReturn(new ArrayList<>(){
      {
        add(StoryDto.builder().build());
      }
    });
    ResponseEntity<List<StoryDto>> response = storyController.searchByTitle("jhon");
    int expected = 1;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(200, response.getStatusCode().value());
  }

  @Test
  void should_returnAListEmpty_when_doesNotExistRecordsInBdSearchByTitle(){
    when(storyService.findFirst5ByTitle(any())).thenReturn(new ArrayList<>());
    ResponseEntity<List<StoryDto>> response = storyController.searchByTitle("jhon");
    int expected = 0;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(404, response.getStatusCode().value());
  }

  @Test
  void should_returnAnException_when_searchByTitleThrowAnException(){
    when(storyService.findFirst5ByTitle(any())).thenThrow(RuntimeException.class);
    try{
      storyController.searchByTitle("jhon");
    }catch(ResponseStatusException e){
      String expected = "500 INTERNAL_SERVER_ERROR \"the {jhon} not found\"";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }

  @Test
  void should_returnAListNotEmpty_when_searchByTagExistRecordsInBd(){
    when(storyService.getByTags(any())).thenReturn(new ArrayList<>(){
      {
        add(StoryDto.builder().build());
      }
    });
    ResponseEntity<List<StoryDto>> response = storyController.searchByTags(new ArrayList<>(){
      {
        add("tag1");
      }
    });
    int expected = 1;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(200, response.getStatusCode().value());
  }

  @Test
  void should_returnAListEmpty_when_doesNotExistRecordsInBdSearchByTag(){
    when(storyService.getByTags(any())).thenReturn(new ArrayList<>());
    ResponseEntity<List<StoryDto>> response = storyController.searchByTags(new ArrayList<>(){
      {
        add("tag1");
      }
    });
    int expected = 0;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(404, response.getStatusCode().value());
  }

  @Test
  void should_returnAnException_when_searchByTagThrowAnException(){
    when(storyService.getByTags(any())).thenThrow(RuntimeException.class);
    try{
      storyController.searchByTags(new ArrayList<>(){
        {
          add("tag1");
        }
      });
    }catch(ResponseStatusException e){
      String expected = "500 INTERNAL_SERVER_ERROR \"the {[tag1]} not found\"";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }
  @Test
  void should_returnAListNotEmpty_when_searchByMonthExistRecordsInBd(){
    when(storyService.getByMonth(any())).thenReturn(new ArrayList<>(){
      {
        add(StoryDto.builder().build());
      }
    });
    ResponseEntity<List<StoryDto>> response = storyController.searchByMonth(Month.NOVEMBER);
    int expected = 1;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(200, response.getStatusCode().value());
  }

  @Test
  void should_returnAListEmpty_when_doesNotExistRecordsInBdSearchByMonth(){
    when(storyService.getByTags(any())).thenReturn(new ArrayList<>());
    ResponseEntity<List<StoryDto>> response = storyController.searchByMonth(Month.NOVEMBER);
    int expected = 0;
    int actual = response.getBody().size();
    assertEquals(expected, actual);
    assertEquals(404, response.getStatusCode().value());
  }

  @Test
  void should_returnAnException_when_searchByMonthThrowAnException(){
    when(storyService.getByMonth(any())).thenThrow(RuntimeException.class);
    try{
      storyController.searchByMonth(Month.NOVEMBER);
    }catch(ResponseStatusException e){
      String expected = "500 INTERNAL_SERVER_ERROR \"the {NOVEMBER} not found\"";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }


}