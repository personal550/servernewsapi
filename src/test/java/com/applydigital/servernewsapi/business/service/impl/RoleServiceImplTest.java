package com.applydigital.servernewsapi.business.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.applydigital.servernewsapi.data.dao.RoleEntity;
import com.applydigital.servernewsapi.data.dto.ERole;
import com.applydigital.servernewsapi.data.dto.RoleDto;
import com.applydigital.servernewsapi.data.repository.RoleRepository;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class RoleServiceImplTest {

  @Spy
  private RoleRepository roleRepository;

  @InjectMocks
  private RoleServiceImpl roleService = new RoleServiceImpl();

  @BeforeEach
  public void setup(){
    /*doNothing()
        .when(roleRepository).save(any(RoleEntity.class));*/
  }

  @Test
  void should_insertAllDataToBd_when_serviceHackerNewsResponseCorrectInformation(){
    roleService.save(RoleDto.builder()
        .id(1)
            .name(ERole.ROLE_ADMIN)
        .build());
    verify(roleRepository, times(1)).save(any(RoleEntity.class));
  }

  @Test
  void should_returnARoll_when_theRoleExistInBD(){
    when(roleRepository.findByName(any())).thenReturn(Optional.ofNullable(RoleEntity.builder()
            .name(ERole.ROLE_ADMIN)
            .id(1)
        .build()));
    Optional<RoleDto> role = roleService.findByName(ERole.ROLE_ADMIN);
    ERole expected = ERole.ROLE_ADMIN;
    ERole actual = role.get().getName();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnNull_when_theRoleDoesNotExistInBD(){
    when(roleRepository.findByName(any())).thenReturn(Optional.ofNullable(null));
    Optional<RoleDto> role = roleService.findByName(ERole.ROLE_ADMIN);
    assertNull(role);
  }

  @Test
  void should_saveARole_when_roleDoesNotExistInDb(){
    Optional<RoleEntity> role = Optional.ofNullable(RoleEntity.builder().name(ERole.DEFAULT).build());
    when(roleRepository.findByName(any())).thenReturn(Optional.ofNullable(RoleEntity.builder().name(null).build()));
    roleService.saveInitialRoles();
  }
}