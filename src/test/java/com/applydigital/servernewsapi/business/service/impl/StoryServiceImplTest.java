package com.applydigital.servernewsapi.business.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.data.dao.TagEntity;
import com.applydigital.servernewsapi.data.dto.Month;
import com.applydigital.servernewsapi.data.repository.StoryRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class StoryServiceImplTest {

  @Mock
  private StoryRepository storyRepository;

  @InjectMocks
  private StoryServiceImpl storyService = new StoryServiceImpl();

  @BeforeEach
  public void setup(){
    when(storyRepository.findById(any())).thenReturn(findById());
    when(storyRepository.findFirst5ByAuthor(any())).thenReturn(findFirst5ByAuthor());
    when(storyRepository.findFirst5ByTitle(any())).thenReturn(findFirst5ByAuthor());
    when(storyRepository.findByCreatedAt(anyInt())).thenReturn(findByCreatedAt());
    when(storyRepository.findByTags(any())).thenReturn(findByCreatedAt());
  }

  @Test
  void should_noUpdateTheRecord_when_recordDoesNotExist(){
    Optional<StoryEntity> find = Optional.ofNullable(null);
    when(storyRepository.findById(any())).thenReturn(find);
    storyService.delete(1l);
    verify(storyRepository, times(1)).findById(1l);
  }

  @Test
  void should_changeStatusToTrue_when_recordExistInDb(){
    storyService.delete(1l);
    verify(storyRepository, times(1)).findById(1l);
  }

  @Test
  void should_returnAListByAuthorLimit5_when_recordExistInDb(){
    List<StoryDto> list = storyService.findFirst5ByAuthor("lop");
    int expected = 1;
    int actual = list.size();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnAListByTitleLimit5_when_recordExistInDb(){
    List<StoryDto> list = storyService.findFirst5ByTitle("lop");
    int expected = 1;
    int actual = list.size();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnAListByMonthLimit5_when_recordExistInDb(){
    List<StoryDto> list = storyService.getByMonth(Month.NOVEMBER);
    int expected = 1;
    int actual = list.size();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnAListByTagsLimit5_when_recordExistInDb(){
    List<StoryDto> list = storyService.getByTags(new ArrayList<>(){
      {
        add("tag 1");
        add("tag 2");
      }
    });
    int expected = 1;
    int actual = list.size();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnFalse_when_recordDoesNotExistInBD(){
    assertFalse(storyService.existIdAndStatus(1l));
  }

  @Test
  void should_returnTrue_when_recordExistInBD(){
    Optional<StoryEntity> entity = findById();
    entity.get().setStatus(true);
    when(storyRepository.findById(any())).thenReturn(entity);
    assertTrue(storyService.existIdAndStatus(1l));
  }

  private List<StoryEntity> findByCreatedAt(){
    return new ArrayList<>(){
      {
        add(StoryEntity.builder()
            .createdAt(LocalDateTime.now())
            .id(1l)
            .author("Jhon")
            .status(false)
            .storyId(11331)
            .storyTitle("title one")
            .tags(new ArrayList<>(){
              {
                add(TagEntity.builder().id(2l).description("description 1").build());
              }
            })
            .build());
      }
    };
  }

  private Optional<List<StoryEntity>> findFirst5ByAuthor(){
    return Optional.ofNullable(new ArrayList<>(){
      {
        add(StoryEntity.builder()
            .createdAt(LocalDateTime.now())
            .id(1l)
            .author("Jhon")
            .status(false)
            .storyId(11331)
            .storyTitle("title one")
            .tags(new ArrayList<>(){
              {
                add(TagEntity.builder().id(2l).description("description 1").build());
              }
            })
            .build());
      }
    });

  }

  private Optional<StoryEntity> findById(){
    return Optional.ofNullable(StoryEntity.builder()
        .createdAt(LocalDateTime.now())
        .id(1l)
        .author("Jhon")
        .status(false)
        .storyId(11331)
        .storyTitle("title one")
        .tags(new ArrayList<>(){
          {
            add(TagEntity.builder().id(2l).description("description 1").build());
          }
        })
        .build());
  }

}