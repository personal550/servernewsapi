package com.applydigital.servernewsapi.business.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.applydigital.servernewsapi.configuration.feign.StoryHackerNewsClient;
import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.data.dao.TagEntity;
import com.applydigital.servernewsapi.data.dto.response.ResponseHackerNews;
import com.applydigital.servernewsapi.data.repository.StoryRepository;
import com.applydigital.servernewsapi.data.repository.TagRepository;
import com.applydigital.servernewsapi.util.ValidateRecordBd;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class HackerNewsServiceImplTest {

  @Mock
  private StoryHackerNewsClient storyClient;

  @Mock
  private StoryRepository storyRepository;

  @Mock
  private TagRepository tagRepository;

  @Mock
  private ValidateRecordBd validateRecordBd;

  @InjectMocks
  private HackerNewsServiceImpl hackerNewsService = new HackerNewsServiceImpl();

  @BeforeEach
  public void setup(){
    when(storyRepository.findByCreatedAt(anyInt())).thenReturn(listFindByCreatedAt());
    when(storyClient.getAll(anyInt(),anyInt())).thenReturn(getAll());
    when(validateRecordBd.excludeIfExistRecordBd(any())).thenReturn(filterIfExistRecordBd());
  }

  @Test
  void should_insertAllDataToBd_when_serviceHackerNewsResponseCorrectInformation(){
    hackerNewsService.execute();
    verify(storyRepository, times(1)).saveAll(any(List.class));
  }


  private List<StoryDto>  filterIfExistRecordBd(){
    return new ArrayList<>(){
      {
        add(StoryDto.builder()
            .id(1l)
            .author("inphovore")
            .created_at("2022-11-15T16:49:13")
            .story_title("Ask HN: Hourly billers, do you bill for only focused work?")
            .story_id(33610147)
            ._tags(new ArrayList<>(){
              {
                add("comment");
                add("author_inphovore");
                add("tory_33610147");
              }
            })
            .build());
        add(StoryDto.builder()
            .id(2l)
            .author("inphovore")
            .created_at("2022-11-15T16:49:13")
            .story_title("Ask HN: Hourly billers, do you bill for only focused work?")
            .story_id(33610147)
            ._tags(new ArrayList<>(){
              {
                add("comment");
                add("author_inphovore");
                add("tory_33610147");
              }
            })
            .build());
      }
    };
  }

  private Optional<ResponseHackerNews> getAll(){
    return Optional.of(ResponseHackerNews
        .builder()
            .nbPages(1)
            .hitsPerPage(10)
            .page(1)
            .hits(new ArrayList<>(){
              {
                add(StoryDto.builder()
                    .id(1l)
                    .author("inphovore")
                    .created_at("2022-11-15T16:49:13")
                    .story_title("Ask HN: Hourly billers, do you bill for only focused work?")
                    .story_id(33610147)
                    ._tags(new ArrayList<>(){
                      {
                        add("comment");
                        add("author_inphovore");
                        add("tory_33610147");
                      }
                    })
                    .build());
              }
            })
        .build());
  }

  private List<StoryEntity> listFindByCreatedAt(){
    return new ArrayList<>(){
      {
        add(StoryEntity.builder()
            .createdAt(LocalDateTime.now())
            .id(1l)
            .author("Jhon")
            .status(false)
            .storyId(11331)
            .storyTitle("title one")
            .tags(new ArrayList<>(){
              {
                add(TagEntity.builder().id(2l).description("description 1").build());
              }
            })
            .build());
        add(StoryEntity.builder()
            .createdAt(LocalDateTime.now())
            .id(1l)
            .author("Jhon")
            .status(false)
            .storyId(11331)
            .storyTitle("title one")
            .tags(new ArrayList<>(){
              {
                add(TagEntity.builder().id(2l).description("description 1").build());
              }
            })
            .build());
      }
    };
  }


}