package com.applydigital.servernewsapi.util;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.data.repository.StoryRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ValidateRecordBdTest {

  @Mock
  private StoryRepository storyRepository;

  @InjectMocks
  private ValidateRecordBd validateRecordBd = new ValidateRecordBd();

  @Test
  void should_returnAListEmpty_when_initialListIsNull(){
    List<StoryDto> response = validateRecordBd.excludeIfExistRecordBd(null);
    int expected = 0;
    int actual = response.size();
    assertEquals(expected, actual);
  }

  @Test
  void should_filterAList_when_theRecordExistIndDb(){
    Optional<StoryEntity> find = Optional.ofNullable(null);
    when(storyRepository.findFirst1ByStoryIdAndAuthorAndCreatedAt(anyInt(), anyString(),
        any())).thenReturn(find);
    List<StoryDto> response = validateRecordBd.excludeIfExistRecordBd(storiesInitial());
    int expected = 2;
    int actual = response.size();
    assertEquals(expected, actual);
  }

  @Test
  void should_filterAList_when_theRecordDoesNotExistIndDb(){
    Optional<StoryEntity> find = Optional.ofNullable(StoryEntity.builder()
        .id(33616593l)
        .author("systemicdanna")
        .createdAt(DateConverter.convertStringToLocalDateTimeObject("2022-11-16T00:26:49.000Z"))
        .build());
    when(storyRepository.findFirst1ByStoryIdAndAuthorAndCreatedAt(anyInt(), anyString(),
        any())).thenReturn(find);
    List<StoryDto> response = validateRecordBd.excludeIfExistRecordBd(storiesInitial());
    int expected = 0;
    int actual = response.size();
    assertEquals(expected, actual);
  }

  private List<StoryDto> storiesInitial(){
    return new ArrayList<>(){
      {
        add(StoryDto.builder()
            .story_id(33616)
            .author("fdsaf")
            .created_at("2022-11-16T00:26:49.000Z")
            .build());
        add(StoryDto.builder()
            .story_id(33615)
            .author("sdaf")
            .created_at("2022-11-16T00:16:19.000Z")
            .build());
      }
    };
  }

}