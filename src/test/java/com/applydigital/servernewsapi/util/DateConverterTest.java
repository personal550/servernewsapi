package com.applydigital.servernewsapi.util;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class DateConverterTest {

  @Test
  void should_returnADateValid_when_formatIsCorrect() {
    LocalDateTime localDateTime = DateConverter.convertStringToLocalDateTimeObject(
        "2022-11-16T00:16:19.000Z");
    assertNotNull(localDateTime);
    int expected = 11;
    int actual = localDateTime.getMonthValue();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnCurrentDat_when_formatIsNotCorrect() {
    LocalDateTime localDateTime = DateConverter.convertStringToLocalDateTimeObject(
        "2022-11-16T00:16:19.da");
    assertNotNull(localDateTime);
  }
}