package com.applydigital.servernewsapi.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Slf4j
public class DateConverter {

  /**
   * This method convert a string with date format to LocalDateTime object
   *
   * @param date
   * @return a new date with a specific format
   */
  public static LocalDateTime convertStringToLocalDateTimeObject(String date) {
    String pattern = "yyyy-MM-dd'T'HH:mm:ss.000'Z'";
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
    try {
      return LocalDateTime.parse(date, dateTimeFormatter);
    } catch (Exception e) {
      log.info(String.format("Error try to format date because : ", e.getMessage()));
      return LocalDateTime.now();
    }
  }
}
