package com.applydigital.servernewsapi.util;

import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.repository.StoryRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Service
@Slf4j
public class ValidateRecordBd {

  @Autowired
  private StoryRepository storyRepository;

  /**
   * This method exclude the records that exist in database
   *
   * @param storiesInitial
   * @return new list of stories
   */
  public List<StoryDto> excludeIfExistRecordBd(List<StoryDto> storiesInitial){
    try{
      return storiesInitial.stream()
          .filter(x -> !storyRepository.findFirst1ByStoryIdAndAuthorAndCreatedAt(x.getStory_id(),
                  x.getAuthor(), DateConverter.convertStringToLocalDateTimeObject(x.getCreated_at()))
              .isPresent())
          .collect(Collectors.toList());

    }catch(Exception e){
      log.info("error try find data in bd because : {}", e.getMessage());
      return new ArrayList<>();
    }
  }

}
