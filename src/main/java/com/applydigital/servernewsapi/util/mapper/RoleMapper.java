package com.applydigital.servernewsapi.util.mapper;

import com.applydigital.servernewsapi.data.dao.RoleEntity;
import com.applydigital.servernewsapi.data.dto.RoleDto;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public class RoleMapper {

  /**
   * This method convert a RoleDto object to a RoleEntity object
   *
   * @param roleDto
   * @return RoleEntity
   */
  public static RoleEntity convertRoleDtoToRoleEntity(RoleDto roleDto){
    return RoleEntity
        .builder()
        .id(roleDto.getId())
        .name(roleDto.getName())
        .build();
  }

  /**
   * This method convert a roleEntity object to a RoleDto object
   *
   * @param roleEntity
   * @return RoleDto
   */
  public static RoleDto convertRoleEntityToRoleDto(RoleEntity roleEntity){
    return RoleDto
        .builder()
        .id(roleEntity.getId())
        .name(roleEntity.getName())
        .build();
  }

  /**
   * This method convert a map of RoleDto to a map of RoleEntity
   *
   * @param listOfRoles
   * @return Set<RoleEntity>
   */
  public static Set<RoleEntity> convertListOfRoleDtoToListOfRoleEntity(Set<RoleDto> listOfRoles){
    Set<RoleEntity> roles = new HashSet<>();
    roles.addAll(listOfRoles.stream()
        .map(role -> convertRoleDtoToRoleEntity(role))
        .collect(Collectors.toSet()));
    return roles;
  }
}
