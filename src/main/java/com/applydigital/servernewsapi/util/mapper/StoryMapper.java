package com.applydigital.servernewsapi.util.mapper;

import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.util.DateConverter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author galindoaguilarf@gmail.com
 * @version 1.0
 */
public class StoryMapper {

  /**
   * This method convert a StoryEntity object to a StoryDto object
   *
   * @param storyEntity
   * @return StoryDto
   */
  public static StoryDto fromStoryEntityToListOfStoryDto(StoryEntity storyEntity) {
    return StoryDto
        .builder()
        .story_id(storyEntity.getStoryId())
        .id(storyEntity.getId())
        .story_title(storyEntity.getStoryTitle())
        ._tags(TagMapper.fromListOfTagsEntityToListOfTagsDto(storyEntity.getTags()))
        .created_at(storyEntity.getCreatedAt().toString())
        .build();
  }

  /**
   * This method convert a list of StoryEntity to a list of StoryDto
   *
   * @param stories
   * @return List<StoryDto>
   */
  public static List<StoryDto> fromListOfStoryEntityToListOfStoryDto(List<StoryEntity> stories) {
    return stories.stream()
        .map(story -> StoryDto
            .builder()
            .id(story.getId())
            .author(story.getAuthor())
            .story_id(story.getStoryId())
            .created_at(story.getCreatedAt().toString())
            .story_title(story.getStoryTitle())
            ._tags(TagMapper.fromListOfTagsEntityToListOfTagsDto(story.getTags()))
            .status(story.getStatus())
            .build())
        .collect(Collectors.toList());
  }

  /**
   * This method convert a list of StoryDto to a list of StoryEntity
   *
   * @param stories
   * @return List<StoryEntity>
   */
  public static List<StoryEntity> fromListOfStoryDtoToListOfStoryEntity(List<StoryDto> stories) {
    return stories.stream()
        .map(story -> StoryEntity
            .builder()
            .id(story.getId())
            .author(story.getAuthor())
            .storyId(story.getStory_id())
            .createdAt(DateConverter.convertStringToLocalDateTimeObject(story.getCreated_at()))
            .storyTitle(story.getStory_title())
            .tags(TagMapper.fromListOfStringToListOfTagsEntity(story.get_tags()))
            .status(false)
            .build())
        .collect(Collectors.toList());
  }


}
