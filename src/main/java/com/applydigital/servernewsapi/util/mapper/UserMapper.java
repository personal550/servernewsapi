package com.applydigital.servernewsapi.util.mapper;

import com.applydigital.servernewsapi.data.UserDto;
import com.applydigital.servernewsapi.data.dao.UserEntity;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public class UserMapper {

  /**
   * This method convert an UserDto object to UserEntity object
   *
   * @param userDto
   * @return UserEntity
   */
  public static UserEntity convertUserDtoToUserEntity(UserDto userDto){
    return UserEntity
        .builder()
        .id(userDto.getId())
        .password(userDto.getPassword())
        .email(userDto.getEmail())
        .username(userDto.getUsername())
        .roles(userDto.getRoles())
        .build();
  }
}
