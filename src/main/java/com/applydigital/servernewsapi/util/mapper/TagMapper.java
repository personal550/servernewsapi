package com.applydigital.servernewsapi.util.mapper;

import com.applydigital.servernewsapi.data.TagDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.data.dao.TagEntity;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public class TagMapper {

  /**
   * This method add a story to specific tag
   *
   * @param story
   * @param tag
   * @return TagEntity
   */
  public static TagEntity createTaEntity(StoryEntity story, TagEntity tag){
    return TagEntity
        .builder()
        .story(story)
        .description(tag.getDescription())
        .build();
  }

  /**
   * This method convert a list of TagEntity to a list TagDto
   *
   * @param tags
   * @return UserEntity
   */
  public static List<TagDto> fromListOfEntityToListOfTagsDto(List<TagEntity> tags) {
    return tags.stream()
        .map(tag -> TagDto
            .builder()
            .description(tag.getDescription())
            .story(StoryMapper.fromStoryEntityToListOfStoryDto(tag.getStory()))
            .build())
        .collect(Collectors.toList());
  }

  /**
   * This method convert a list of String to a list TagEntity
   *
   * @param tags
   * @return List<TagEntity>
   */
  public static List<TagEntity> fromListOfStringToListOfTagsEntity(List<String> tags) {

    return tags.stream()
        .map(tag -> TagEntity
            .builder()
            .description(tag)
            .build())
        .collect(Collectors.toList());
  }

  /**
   * This method convert a list of TagEntity to a list String
   *
   * @param tags
   * @return List<String>
   */
  public static List<String> fromListOfTagsEntityToListOfTagsDto(List<TagEntity> tags) {
    return tags.stream()
        .map(tag -> tag.getDescription())
        .collect(Collectors.toList());
  }
}
