package com.applydigital.servernewsapi.web.controller;

import com.applydigital.servernewsapi.business.service.RoleService;
import com.applydigital.servernewsapi.business.service.UserService;
import com.applydigital.servernewsapi.configuration.security.jwt.JwtUtils;
import com.applydigital.servernewsapi.configuration.security.services.UserDetailsImpl;
import com.applydigital.servernewsapi.data.UserDto;
import com.applydigital.servernewsapi.data.dto.ERole;
import com.applydigital.servernewsapi.data.dto.RoleDto;
import com.applydigital.servernewsapi.data.dto.request.RequestLogin;
import com.applydigital.servernewsapi.data.dto.request.RequestSignUp;
import com.applydigital.servernewsapi.data.dto.response.JwtResponse;
import com.applydigital.servernewsapi.data.dto.response.MessageResponse;
import com.applydigital.servernewsapi.util.mapper.RoleMapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author galindoaguilarf
 * @version 1.0
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserService userService;

  @Autowired
  RoleService roleService;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/authenticate")
  public ResponseEntity<JwtResponse> authenticateUser(@RequestBody RequestLogin loginRequest) {
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(JwtResponse.builder()
        .id(userDetails.getId())
        .email(userDetails.getEmail())
        .roles(roles)
        .username(userDetails.getUsername())
        .token(jwt)
        .type("Bearer")
        .build());
  }

  @PostMapping("/signup")
  public ResponseEntity<MessageResponse> signup(@RequestBody RequestSignUp signUpRequest) {

    if (userService.existsByUsername(signUpRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(MessageResponse.builder().message("Error: Username is already taken!").build());
    }
    if (userService.existsByEmail(signUpRequest.getEmail())) {
      return ResponseEntity
          .badRequest()
          .body(MessageResponse.builder().message("Error: Email is already in use!").build());
    }

    UserDto user = UserDto
        .builder()
        .username(signUpRequest.getUsername())
        .email(signUpRequest.getEmail())
        .password(encoder.encode(signUpRequest.getPassword()))
        .build();
    Set<RoleDto> roles = new HashSet<>();
    RoleDto userRole = roleService.findByName(ERole.ROLE_ADMIN)
        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
    roles.add(userRole);
    user.setRoles(RoleMapper.convertListOfRoleDtoToListOfRoleEntity(roles));
    userService.saveUser(user);
    return ResponseEntity.ok(
        MessageResponse.builder().message(
                String.format("user %s was registered successfully!", signUpRequest.getUsername()))
            .build());
  }
}
