package com.applydigital.servernewsapi.web.controller;

import com.applydigital.servernewsapi.business.service.HackerNewsService;
import com.applydigital.servernewsapi.business.service.StoryService;
import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.data.dto.Month;
import com.applydigital.servernewsapi.data.repository.StoryRepository;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.swing.text.html.parser.Entity;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${api.prefix}${api.version}story")
@Slf4j
public class StoryController {

  @Autowired
  private HackerNewsService hackerNewsService;
  @Autowired
  private StoryService storyService;

  @GetMapping(value = "/refresh")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity populateBd() {
    hackerNewsService.execute();
    return ResponseEntity.ok().build();
  }

  @ApiOperation(value = "Find all news.", hidden = true)
  @GetMapping(value = "/")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<List<StoryDto>> findAll() {
    return new ResponseEntity<>(storyService.findAll(), HttpStatus.OK);
  }


  @DeleteMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity deleteById(@Valid @PathVariable(value = "id") Long id) {
    try {
      if (!storyService.existIdAndStatus(id)) {
        storyService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } else {
        return new ResponseEntity<>(String.format("the id {%d} was deleting before", id),
            HttpStatus.NOT_FOUND
        );
      }
    } catch (Exception e) {
      log.info("error found : " + e.getMessage());
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, String.format("the id {%d} cant delete", id),
          e.getCause());
    }
  }

  @GetMapping("/author/{author}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<List<StoryDto>> searchByAuthor(@Valid @PathVariable String author) {
    try {
      List<StoryDto> authors = storyService.findFirst5ByAuthor(author);
      if (authors.isEmpty()) {
        return new ResponseEntity<>(authors, HttpStatus.NOT_FOUND);
      } else {
        return new ResponseEntity<>(authors, HttpStatus.OK);
      }
    } catch (Exception e) {
      log.info("error found : " + e.getMessage());
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, String.format("the {%s} not found", author),
          e.getCause());
    }
  }

  @GetMapping("/title/{title}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<List<StoryDto>> searchByTitle(@Valid @PathVariable String title) {
    try {
      List<StoryDto> titles = storyService.findFirst5ByTitle(title);
      if (titles.isEmpty()) {
        return new ResponseEntity<>(titles, HttpStatus.NOT_FOUND);
      } else {
        return new ResponseEntity<>(titles, HttpStatus.OK);
      }
    } catch (Exception e) {
      log.info("error found : " + e.getMessage());
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, String.format("the {%s} not found", title),
          e.getCause());
    }
  }

  @GetMapping("/tags/{tags}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<List<StoryDto>> searchByTags(@Valid @PathVariable List<String> tags) {
    try {
      log.info(tags.toString());
      List<StoryDto> stories = storyService.getByTags(tags);
      if (stories.isEmpty()) {
        return new ResponseEntity<>(stories, HttpStatus.NOT_FOUND);
      } else {
        return new ResponseEntity<>(stories, HttpStatus.OK);
      }

    } catch (Exception e) {
      log.info("error found : " + e.getMessage());
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, String.format("the {%s} not found", tags.toString()),
          e.getCause());
    }
  }

  @GetMapping("/month/{month}")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<List<StoryDto>> searchByMonth(@Valid @PathVariable Month month) {
    try {
      log.info("month : {}", month.ordinal());
      List<StoryDto> stories = storyService.getByMonth(month);
      if(stories.isEmpty()){
        return new ResponseEntity<>(stories, HttpStatus.NOT_FOUND);
      }else{
        return new ResponseEntity<>(stories, HttpStatus.OK);
      }
    } catch (Exception e) {
      log.info("error found : " + e.getMessage());
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, String.format("the {%s} not found", month.name()),
          e.getCause());
    }
  }

}
