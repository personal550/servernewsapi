package com.applydigital.servernewsapi.business.service;

import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dto.Month;
import java.util.List;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public interface StoryService {
  List<StoryDto> findAll();

  void delete(Long id);

  List<StoryDto> findFirst5ByAuthor(String author);

  List<StoryDto> findFirst5ByTitle(String title);

  List<StoryDto> getByMonth(Month month);

  List<StoryDto> getByTags(List<String> tags);

  Boolean existIdAndStatus(Long id);

}
