package com.applydigital.servernewsapi.business.service.impl;


import com.applydigital.servernewsapi.business.service.StoryService;
import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.data.dto.Month;
import com.applydigital.servernewsapi.data.repository.StoryRepository;
import com.applydigital.servernewsapi.util.mapper.StoryMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Service
@Slf4j
public class StoryServiceImpl implements StoryService {

  @Autowired
  private StoryRepository storyRepository;

  @Override
  public List<StoryDto> findAll() {
    Iterable<StoryEntity> storiesIterable = storyRepository.findAll();
    List<StoryEntity> storiesList = Streamable.of(storiesIterable).toList();
    return StoryMapper.fromListOfStoryEntityToListOfStoryDto(storiesList);
  }


  /**
   * This method change a status in database of a record
   *
   * @param id
   */
  @Override
  public void delete(Long id) {
    Optional<StoryEntity> storyEntity = storyRepository.findById(id);
    if(storyEntity.isPresent()){
      StoryEntity story =storyEntity.get();
      story.setStatus(true);
      storyRepository.save(story);
    }
  }

  /**
   * This method find first five records by author
   *
   * @param author
   * @return List<StoryDto>
   */
  @Override
  public List<StoryDto> findFirst5ByAuthor(String author) {
    return StoryMapper.fromListOfStoryEntityToListOfStoryDto(
        storyRepository.findFirst5ByAuthor(author).orElse(new ArrayList<>()));
  }

  /**
   * This method find first five records by title
   *
   * @param title
   * @return List<StoryDto>
   */
  @Override
  public List<StoryDto> findFirst5ByTitle(String title) {
    return StoryMapper.fromListOfStoryEntityToListOfStoryDto(
        storyRepository.findFirst5ByTitle(title).orElse(new ArrayList<>()));
  }

  /**
   * This method find first five records by month
   *
   * @param month
   * @return List<StoryDto>
   */
  @Override
  public List<StoryDto> getByMonth(Month month) {
    return StoryMapper.fromListOfStoryEntityToListOfStoryDto(
        storyRepository.findByCreatedAt(month.ordinal() + 1));
  }

  /**
   * This method find first five records by tags
   *
   * @param tags
   * @return List<StoryDto>
   */
  @Override
  public List<StoryDto> getByTags(List<String> tags) {
    String tagsSeparatedByComma = String.join(",", tags);
    log.info("tags : {}", tagsSeparatedByComma);
    return StoryMapper.fromListOfStoryEntityToListOfStoryDto(
        storyRepository.findByTags(tagsSeparatedByComma));
  }

  /**
   * This method validate if a story exist
   *
   * @param id
   * @return boolean
   */
  @Override
  public Boolean existIdAndStatus(Long id) {
    Optional<StoryEntity> storyEntity = storyRepository.findById(id);
    try{
      if(storyEntity.isPresent() && storyEntity.get().getStatus()){
        return true;
      }
    }catch(Exception e){
      log.info("error try retrieve a entity from DB because: "+ e.getMessage());
    }
    return false;
  }
}