package com.applydigital.servernewsapi.business.service.impl;

import com.applydigital.servernewsapi.business.service.HackerNewsService;
import com.applydigital.servernewsapi.configuration.feign.StoryHackerNewsClient;
import com.applydigital.servernewsapi.data.StoryDto;
import com.applydigital.servernewsapi.data.dao.StoryEntity;
import com.applydigital.servernewsapi.data.dto.response.ResponseHackerNews;
import com.applydigital.servernewsapi.data.repository.StoryRepository;
import com.applydigital.servernewsapi.data.repository.TagRepository;
import com.applydigital.servernewsapi.util.ValidateRecordBd;
import com.applydigital.servernewsapi.util.mapper.StoryMapper;
import com.applydigital.servernewsapi.util.mapper.TagMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
/**
 * @author galindoaguilarf
 * @version 1.0
 */
public class HackerNewsServiceImpl implements HackerNewsService {

  @Autowired
  private StoryHackerNewsClient storyClient;

  @Autowired
  private StoryRepository storyRepository;

  @Autowired
  private TagRepository tagRepository;

  @Autowired
  private ValidateRecordBd validateRecordBd;

  @Value("${hitsPerPage}")
  private int hitsPerPage;

  /**
   * This method execute hacker news api and save all information in the database
   *
   */
  @Override
  public void execute() {
    Optional<ResponseHackerNews> responseOptional = storyClient.getAll(0, hitsPerPage);
    List<StoryDto> storiesFull = new ArrayList<>();
    if (responseOptional.isPresent()) {
      ResponseHackerNews response = responseOptional.get();
      storiesFull.addAll(Optional.ofNullable(response.getHits()).orElse(new ArrayList<>()));
      IntStream it = IntStream.range(1, response.getNbPages());
      try {
        it.forEach(x -> storiesFull.addAll(storyClient.getAll(x, hitsPerPage).orElse(
            ResponseHackerNews.builder().build()).getHits()));
      } catch (Exception e) {
        log.info("The api hacker news not found records because: {}", e.getMessage());
      }
    } else {
      log.info("The api hacker news not found records");
    }
    List<StoryDto> storiesFiltered = validateRecordBd.excludeIfExistRecordBd(storiesFull);

    List<StoryEntity> storyEntities;
    if (!storiesFiltered.isEmpty()) {
      storyEntities = StoryMapper.fromListOfStoryDtoToListOfStoryEntity(
          storiesFiltered);
      storyRepository.saveAll(storyEntities);
      storyEntities.stream()
          .forEach((story) -> story.getTags().stream()
              .forEach(tag -> tagRepository.save(TagMapper.createTaEntity(story, tag))));
    }
  }
}
