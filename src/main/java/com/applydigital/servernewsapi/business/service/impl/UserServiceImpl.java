package com.applydigital.servernewsapi.business.service.impl;

import com.applydigital.servernewsapi.business.service.UserService;
import com.applydigital.servernewsapi.data.UserDto;
import com.applydigital.servernewsapi.data.repository.UserRepository;
import com.applydigital.servernewsapi.util.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  /**
   * This method save a user inside database
   *
   * @param user
   */
  @Override
  public void saveUser(UserDto user) {
    userRepository.save(UserMapper.convertUserDtoToUserEntity(user));
  }

  /**
   * This method validate if a user exist by email
   *
   * @param email
   * @return boolean
   */
  @Override
  public boolean existsByEmail(String email) {
    return userRepository.existsByEmail(email);
  }

  /**
   * This method validate if exist a user by username
   *
   * @param username
   * @return boolean
   */
  @Override
  public boolean existsByUsername(String username) {
    return userRepository.existsByUsername(username);
  }
}
