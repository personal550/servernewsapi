package com.applydigital.servernewsapi.business.service.impl;

import com.applydigital.servernewsapi.business.service.RoleService;
import com.applydigital.servernewsapi.data.dto.ERole;
import com.applydigital.servernewsapi.data.dao.RoleEntity;
import com.applydigital.servernewsapi.data.dto.RoleDto;
import com.applydigital.servernewsapi.data.repository.RoleRepository;
import com.applydigital.servernewsapi.util.mapper.RoleMapper;
import java.util.Optional;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
/**
 * @author galindoaguilarf
 * @version 1.0
 */
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleRepository roleRepository;

  /**
   * This method save a role inside the database
   *
   * @param role
   */
  @Override
  public void save(RoleDto role) {
    roleRepository.save(RoleMapper.convertRoleDtoToRoleEntity(role));
  }

  /**
   * This method find all records by name
   *
   * @param role
   * @return Optional<RoleEntity>
   */
  @Override
  public Optional<RoleDto> findByName(ERole role) {
    Optional<RoleEntity> roleEntity = roleRepository.findByName(role);
    if (roleEntity.isPresent()) {
      return Optional.ofNullable(RoleMapper.convertRoleEntityToRoleDto(roleEntity.get()));
    } else {
      return null;
    }
  }

  /**
   * This method save a role inside database
   *
   */
  @PostConstruct
  public void saveInitialRoles() {
    RoleEntity roleEntity = RoleEntity.builder().name(ERole.ROLE_ADMIN).build();
    if(!roleRepository.findByName(roleEntity.getName()).isPresent()){
      roleRepository.save(roleEntity);
    }
    log.info("roles were inserted successfully");
  }
}
