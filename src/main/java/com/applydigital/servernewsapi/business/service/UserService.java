package com.applydigital.servernewsapi.business.service;

import com.applydigital.servernewsapi.data.UserDto;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public interface UserService {

  void saveUser(UserDto user);

  boolean existsByEmail(String email);

  boolean existsByUsername(String username);
}
