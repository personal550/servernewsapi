package com.applydigital.servernewsapi.business.service;

import com.applydigital.servernewsapi.data.dto.ERole;
import com.applydigital.servernewsapi.data.dto.RoleDto;
import java.util.Optional;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public interface RoleService {
    void save(RoleDto role);
    Optional<RoleDto> findByName(ERole role);
}
