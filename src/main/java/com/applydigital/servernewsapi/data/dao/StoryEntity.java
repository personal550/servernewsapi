package com.applydigital.servernewsapi.data.dao;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Entity
@Table(name="story")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StoryEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_story")
  private Long id;

  @Column(name = "story_id")
  private Integer storyId;

  private String author;

  private String title;

  @OneToMany(mappedBy="story", cascade =CascadeType.ALL)
  private List<TagEntity> tags;

  @Column(name = "created_at", columnDefinition = "TIMESTAMP")
  private LocalDateTime createdAt;

  @Column(name = "story_title")
  private String storyTitle;

  private Boolean status;

  @Override
  public String toString() {
    return "StoryEntity{" +
        "id=" + id +
        ", storyId=" + storyId +
        ", author='" + author + '\'' +
        ", title='" + title + '\'' +
        ", tags=" + tags +
        ", createdAt=" + createdAt +
        ", storyTitle='" + storyTitle + '\'' +
        ", status=" + status +
        '}';
  }
}
