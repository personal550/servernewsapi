package com.applydigital.servernewsapi.data;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StoryDto implements Serializable {

  private Long id;
  private Integer story_id;
  private String author;
  private List<String> _tags;
  private String title;
  private String created_at;
  private String story_title;
  private Boolean status;
}
