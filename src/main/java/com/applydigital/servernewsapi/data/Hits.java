package com.applydigital.servernewsapi.data;

import java.util.List;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Hits {

  private List<StoryDto> stories;
}
