package com.applydigital.servernewsapi.data.repository;

import com.applydigital.servernewsapi.data.dao.UserEntity;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
  Optional<UserEntity> findByUsername(String username);

  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);
}
