package com.applydigital.servernewsapi.data.repository;

import com.applydigital.servernewsapi.data.dao.TagEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Repository
public interface TagRepository extends CrudRepository<TagEntity, Long> {

  TagEntity findFirst5ByDescription(String description);
}
