package com.applydigital.servernewsapi.data.repository;

import com.applydigital.servernewsapi.data.dao.StoryEntity;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public interface StoryRepository extends CrudRepository<StoryEntity, Long> {

  Optional<StoryEntity> findFirst1ByStoryIdAndAuthorAndCreatedAt(Integer storyId, String author, LocalDateTime date);

  @Query(value =
      "SELECT s.* FROM story s WHERE s.author = :author and s.status = false limit 5", nativeQuery = true)
  Optional<List<StoryEntity>> findFirst5ByAuthor(String author);

  @Query(value =
      "SELECT s.* FROM story s WHERE s.title = :title and s.status = false limit 5", nativeQuery = true)
  Optional<List<StoryEntity>> findFirst5ByTitle(String title);

  @Query(value = "SELECT s.* FROM story s WHERE EXTRACT(MONTH FROM s.created_at) = :month and s.status = false limit 5",
      nativeQuery = true)
  List<StoryEntity> findByCreatedAt(@Param("month") int month);

  @Query(value =
      "SELECT s.* FROM story s "
          + "LEFT JOIN tag t "
          + "ON s.id_story = t.id_story "
          + "WHERE t.description in (:tags) and s.status = false limit 5", nativeQuery = true)
  List<StoryEntity> findByTags(@Param("tags") String tags);

}

