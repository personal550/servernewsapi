package com.applydigital.servernewsapi.data.repository;

import com.applydigital.servernewsapi.data.dto.ERole;
import com.applydigital.servernewsapi.data.dao.RoleEntity;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Repository
public interface RoleRepository extends CrudRepository<RoleEntity, Long> {
  Optional<RoleEntity> findByName(ERole name);
}
