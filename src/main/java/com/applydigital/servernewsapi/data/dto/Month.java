package com.applydigital.servernewsapi.data.dto;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public enum Month {
  JANUARY,
  FEBRUARY,
  MARCH,
  APRIL,
  MAY,
  JUNE,
  JULY,
  AUGUST,
  SEPTEMBER,
  OCTOBER,
  NOVEMBER,
  DECEMBER
}