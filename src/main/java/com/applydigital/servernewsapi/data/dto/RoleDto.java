package com.applydigital.servernewsapi.data.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@AllArgsConstructor
@Data
@Builder
public class RoleDto {
  private Integer id;
  private ERole name;
}
