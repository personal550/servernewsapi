package com.applydigital.servernewsapi.data.dto.response;

import com.applydigital.servernewsapi.data.StoryDto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseHackerNews implements Serializable {

  private int page;
  private int nbPages;
  private int hitsPerPage;
  private List<StoryDto> hits = new ArrayList<>();
}

