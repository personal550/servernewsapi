package com.applydigital.servernewsapi.data.dto.request;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@Data
public class RequestDeleteStory implements Serializable {

  @NotNull(message = "id is is required")
  private long id;
}
