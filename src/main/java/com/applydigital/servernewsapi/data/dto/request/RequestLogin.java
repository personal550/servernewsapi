package com.applydigital.servernewsapi.data.dto.request;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@Data
public class RequestLogin implements Serializable {

  @NotBlank(message = "username is required")
  private String username;

  @NotBlank(message = "password is required")
  private String password;
}
