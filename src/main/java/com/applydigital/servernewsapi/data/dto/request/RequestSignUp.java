package com.applydigital.servernewsapi.data.dto.request;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.*;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@Data
public class RequestSignUp implements Serializable {
  @NotBlank(message = "username is required")
  @Size(min = 3, max = 20)
  private String username;

  @NotBlank(message = "email is required")
  @Size(max = 50)
  @Email
  private String email;

  @NotBlank(message = "password is required")
  @Size(min = 6, max = 40)
  private String password;

}
