package com.applydigital.servernewsapi.data.dto.response;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@AllArgsConstructor
@Data
public class JwtResponse implements Serializable {
  private String token;
  private String type;
  private Long id;
  private String username;
  private String email;
  private List<String> roles;

  public JwtResponse() {
  }
}
