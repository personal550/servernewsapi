package com.applydigital.servernewsapi.data.dto.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@Data
@AllArgsConstructor
public class MessageResponse implements Serializable {
  private String message;
  public MessageResponse() {
  }
}
