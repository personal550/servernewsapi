package com.applydigital.servernewsapi.data.dto;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public enum ERole {
  ROLE_ADMIN,
  DEFAULT
}
