package com.applydigital.servernewsapi.data.dto.request;

import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.ToString;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@ToString
public class RequestSearchStory implements Serializable {
  private String author;
  private List<String> tags;
  private String title;
  private String month;
}
