package com.applydigital.servernewsapi.data;

import com.applydigital.servernewsapi.data.dao.RoleEntity;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@Data
public class UserDto implements UserDetails {
  private Long id;
  private String username;
  private String password;
  private String name;
  private String email;
  private Set<RoleEntity> roles = new HashSet<>();

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
