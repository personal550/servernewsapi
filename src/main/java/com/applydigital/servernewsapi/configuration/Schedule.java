package com.applydigital.servernewsapi.configuration;

import com.applydigital.servernewsapi.business.service.HackerNewsService;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Component
@Slf4j
public class Schedule {

  @Autowired
  private HackerNewsService hackerNewsService;

  /**
   * This method execute the cron with a expression defined in application properties
   */
  @Scheduled(cron = "${cron.expression}")
  public void scheduleTaskExecuteEveryHour() {
    try {
      hackerNewsService.execute();
      log.info("the hacker news api was executed successfully");
    } catch (Exception e) {
      log.info(String.format("Scheduled task could not execute successfully because : %s", e.getMessage()));
    }
  }

  /**
   * This method execute the cron when the object is ready
   *
   */
  @PostConstruct
  public void init(){
    scheduleTaskExecuteEveryHour();
  }
}
