package com.applydigital.servernewsapi.configuration.feign;

import com.applydigital.servernewsapi.data.dto.response.ResponseHackerNews;
import java.util.Optional;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@FeignClient(name = "stories", url = "${host.url.service}")
public interface StoryHackerNewsClient {

  @RequestMapping(method = RequestMethod.GET, value = "${host.endpoint.hacker.news}")
  Optional<ResponseHackerNews> getAll(@RequestParam(value = "page") int page, @RequestParam(value = "hitsPerPage") int hitsPerPage);
}
