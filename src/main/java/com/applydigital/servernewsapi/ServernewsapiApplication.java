package com.applydigital.servernewsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@SpringBootApplication
@EnableFeignClients
@EnableScheduling
@EnableSwagger2
public class ServernewsapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServernewsapiApplication.class, args);
	}

}
